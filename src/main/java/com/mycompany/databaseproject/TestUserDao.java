/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author chana
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        //User usertwo = userDao.get(2);
        //System.out.println(usertwo);

        //User newUser = new User("user3", "password", 2, "F");
        //User insertedUser = userDao.save(newUser);
        //System.out.println(insertedUser);
        //insertedUser.setGender("M");
        //usertwo.setGender("F");
        //userDao.update(usertwo);
        //User updateUser = userDao.get(usertwo.getId());
        //System.out.println(updateUser);
        //userDao.delete(usertwo);
        //for (User u : userDao.getAll()) {
        //    System.out.println(u);
        //}
        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc")) {
            System.out.println(u);
        }

        DatabaseHelper.clos();
    }
}
